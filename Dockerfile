#### Use latest Ubuntu
FROM ubuntu:focal

# Update base container install
RUN apt-get update
#RUN apt-get upgrade -y

ENV TZ 'GB'

RUN echo $TZ > /etc/timezone && \
    apt-get install -y tzdata && \
    rm /etc/localtime && \
    ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && \
    dpkg-reconfigure -f noninteractive tzdata && \
    apt-get clean

# Install GDAL dependencies
RUN apt-get install -y python3-pip libgdal-dev locales

# Ensure locales configured correctly
RUN locale-gen en_GB.UTF-8
ENV LC_ALL='en_GB.utf8'

# Set python aliases for python3
RUN echo 'alias python=python3' >> ~/.bashrc
RUN echo 'alias pip=pip3' >> ~/.bashrc

# Update C env vars so compiler can find gdal
ENV CPLUS_INCLUDE_PATH=/usr/include/gdal
ENV C_INCLUDE_PATH=/usr/include/gdal

# This will install latest version of GDAL
RUN pip3 install GDAL==3.0.4
RUN pip3 install --no-cache-dir "uvicorn[standard]" gunicorn
RUN pip3 install --no-cache-dir fastapi
RUN pip3 install --no-cache SQLAlchemy
RUN pip3 install --no-cache psycopg2-binary


#Copy start and reload scripts
COPY ./scripts/start.sh /start.sh
RUN chmod +x /start.sh

COPY ./scripts/gunicorn_conf.py /gunicorn_conf.py

COPY ./scripts/start-reload.sh /start-reload.sh
RUN chmod +x /start-reload.sh

COPY ./app /app

ENV PYTHONPATH=/app

EXPOSE 80
CMD ["/start.sh"]
