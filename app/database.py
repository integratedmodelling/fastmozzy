from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import os
postgres_user = os.environ['POSTGRES_USER']
postgres_pass = os.environ['POSTGRES_PASSWORD']
postgres_host = os.environ['POSTGRES_HOST']
postgres_db = os.environ['POSTGRES_DB']


def get_engine():
    SQLALCHEMY_DATABASE_URL = 'postgresql://%s:%s@%s' % (
        postgres_user, postgres_pass, postgres_host)
    
    test_engine = create_engine(SQLALCHEMY_DATABASE_URL, execution_options={
                                'isolation_level': 'AUTOCOMMIT'})
    
    search = "SELECT 1 FROM pg_database WHERE datname='%s'" % postgres_db
    
    test_engine.connect()
    
    if len(test_engine.execute(search).fetchall()) != 0:
        SQLALCHEMY_DATABASE_URL = 'postgresql://%s:%s@%s/%s' % (
            postgres_user, postgres_pass, postgres_host, postgres_db)
        engine = create_engine(SQLALCHEMY_DATABASE_URL)
        return engine
    
    create = "CREATE DATABASE %s ENCODING '%s ' TEMPLATE template0" % (
        postgres_db, 'utf-8')
    test_engine.execute(create)
    SQLALCHEMY_DATABASE_URL = 'postgresql://%s:%s@%s/%s' % (
        postgres_user, postgres_pass, postgres_host, postgres_db)
    engine = create_engine(SQLALCHEMY_DATABASE_URL)
    return engine


engine = get_engine()

SessionLocal = sessionmaker(autocommit=False,
                            autoflush=False,
                            bind=engine)

Base = declarative_base()
