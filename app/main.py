from typing import List
from fastapi import Depends, FastAPI, HTTPException
from sqlalchemy.orm import Session
import crud, models, schemas
from database import SessionLocal, engine

models.Base.metadata.create_all(bind=engine)
app = FastAPI()

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.post('/datastores/', response_model=(schemas.DataStore))
def create_data_store(ds: schemas.DataStore, db: Session=Depends(get_db)):
    db_ds = crud.get_data_store_by_name(db, name=(ds.name))
    if db_ds:
        raise HTTPException(status_code=400, detail='Name already registered')
    return crud.create_data_store(db=db, ds=ds)


@app.get('/datastores/', response_model=(List[schemas.DataStore]))
def read_data_store(skip: int=0, limit: int=100, db: Session=Depends(get_db)):
    dss = crud.get_data_stores(db, skip=skip, limit=limit)
    return dss


@app.get('/datastores/{ds_id}', response_model=(schemas.DataStore))
def read_data_store(ds_id: int, db: Session=Depends(get_db)):
    db_ds = crud.get_data_store(db, user_id=user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail='DataStore not found')
    return db_ds
