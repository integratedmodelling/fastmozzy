from osgeo import gdal
from osgeo import osr
from xml.etree.ElementTree import Element, SubElement, Comment, tostring
from xml.etree import ElementTree
from xml.dom import minidom

def SpatialReference(dataset):
    pszProjection = dataset.GetProjectionRef()
    if pszProjection is not None:
        hSRS = osr.SpatialReference()
        if hSRS.ImportFromWkt(pszProjection) == gdal.CE_None:
            return hSRS


def DataSetToCoverage(dataset, store, workspace):
    coverage = Element('coverage')
    child = SubElement(coverage, 'name')
    child.text = store
    child = SubElement(coverage, 'nativeName')
    child.text = store
    child = SubElement(coverage, 'namespace')
    grandChild = SubElement(child, 'name')
    grandChild.text = workspace
    child = SubElement(coverage, 'title')
    child.text = store
    hSRS = SpatialReference(dataset)
    child = SubElement(coverage, 'nativeCRS')
    child.text = hSRS.ExportToWkt()
    if hSRS.IsGeographic() == 1:
        cstype = 'GEOGCS'
    else:
        cstype = 'PROJCS'
    srs = '%s:%s' % (hSRS.GetAuthorityName(cstype), hSRS.GetAuthorityCode(cstype))
    child = SubElement(coverage, 'srs')
    child.text = srs
    child = SubElement(coverage, 'requestSRS')
    grandChild = SubElement(child, 'string')
    grandChild.text = srs
    child = SubElement(coverage, 'responseSRS')
    grandChild = SubElement(child, 'string')
    grandChild.text = srs
    child = SubElement(coverage, 'enabled')
    child.text = 'true'
    child = SubElement(coverage, 'supportedFormats')
    grandChild = SubElement(child, 'string')
    grandChild.text = 'S3GeoTiff'
    grandChild = SubElement(child, 'string')
    grandChild.text = 'GEOTIFF'
    grandChild = SubElement(child, 'string')
    grandChild.text = 'ImageMosaic'
    child = SubElement(coverage, 'interpolationMethods')
    grandChild = SubElement(child, 'string')
    grandChild.text = 'nearest neighbor'
    grandChild = SubElement(child, 'string')
    grandChild.text = 'bilinear'
    grandChild = SubElement(child, 'string')
    grandChild.text = 'bicubic'
    child = SubElement(coverage, 'defaultInterpolationMethod')
    child.text = 'nearest neighbor'
    return coverage


def prettify(elem):
    rough_string = tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent='  ')


if __name__ == '__main__':
    ds = gdal.Open('/vsicurl/https://modis-vi-nasa.s3-us-west-2.amazonaws.com/MOD13A1.006/2018.01.01.tif')
    print(prettify(DataSetToCoverage(ds, 'modisvi', 'test')))
