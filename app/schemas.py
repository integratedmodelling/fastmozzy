from typing import List, Optional

from pydantic import BaseModel


class DataStore(BaseModel):
    id: int
    name: str
    user: str
    port: int
    host: str
    driver: str
    SPI: str
    fetch_size: int
    max_connections: int
    min_connections: int
    validate_connections: bool
    Loose_bbox: bool
    Expose_primary_key: bool
    Max_open_prepared_statements: int
    Estimated_extends: bool
    Connection_timeout: int

    class Config:
        orm_mode = True


class IndexerProperties(BaseModel):
    id: int
    Name: str
    Cog: bool
    PropertyCollectors: str
    TimeAttribute: str
    Schema: str
    CanBeEmpty: bool

    class Config:
        orm_mode = True


class TimeRegex(BaseModel):
    id: int
    Name: str
    regex: str

    class Config:
        orm_mode = True