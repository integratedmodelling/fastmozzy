from typing import Optional
from fastapi import FastAPI
from pydantic import BaseModel
from osgeo import gdal
from osgeo import osr
from utils import GDALUtils
import xml.etree.ElementTree as ET
gdal.SetConfigOption('AWS_REGION', 'eu-central-1')
gdal.SetConfigOption('AWS_NO_SIGN_REQUEST', 'yes')

class Strategy(BaseModel):
    name: str
    timeregex = None
    timeregex: Optional[str]
    cog = True
    cog: Optional[bool]


class CoverageRequest(BaseModel):
    url: str
    workspace: str
    store: str
    strategy: Strategy


def ProcessRequest(cr: CoverageRequest):
    hDataset = gdal.Open('/vsicurl/' + cr.url)


def ProcessDStoCoverage(dataset, strategy):
    data = ET.Element('')
    projection = GDALUtils.GeoProjection(dataset)
