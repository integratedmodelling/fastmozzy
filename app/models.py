from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from database import Base

class DataStore(Base):
    __tablename__ =  "datastore"
    id =  Column(Integer, primary_key=True, index=True)
    name =  Column(String, unique=True, index=True)
    user =  Column(String, unique=False, index=False)
    port =  Column(Integer, unique=False, index=False)
    host =  Column(String, unique=False, index=False)
    driver =  Column(String, unique=False, index=False)
    SPI =  Column(String, unique=False, index=False)
    fetch_size =  Column(Integer, unique=False, index=False)
    max_connections =  Column(Integer, unique=False, index=False)
    min_connections =  Column(Integer, unique=False, index=False)
    validate_connections =  Column(Boolean, unique=False, index=False)
    Loose_bbox =  Column(Boolean, unique=False, index=False)
    Expose_primary_key =  Column(Boolean, unique=False, index=False)
    Max_open_prepared_statements=Column(Integer, unique=False, index=False)
    Estimated_extends =  Column(Boolean, unique=False, index=False)
    Connection_timeout = Column(Integer, unique=False, index=False)

class IndexerProperties(Base): 
    __tablename__ =  "indexer_properties"
    id =  Column(Integer, primary_key=True, index=True)
    Name =  Column(String, unique=True, index=True)
    Cog =  Column(Boolean, unique=False, index=False)
    PropertyCollectors =  Column(String, unique=False, index=False)
    TimeAttribute =  Column(String, unique=False, index=False)
    Schema =  Column(String, unique=False, index=False)
    CanBeEmpty =  Column(Boolean, unique=False, index=False)

class TimeRegex(Base):
    __tablename__ =  "time_regex"
    id =  Column(Integer, primary_key=True, index=True)
    name =  Column(String, unique=True, index=True)
    regex =  Column(String, unique=False, index=False)